<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;
class LoginAdmin extends Controller
{
    //
    public function login(){
        return view('admin.login');
    }
    public function registration(){
        return view('admin.registration');
    }
    public function add_registration(Request $request){
        $data = array();
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['password']= md5($request->password);
        DB::table('users')->insertGetId($data);
        Session::put('message','Bạn đã đăng ký thành công');
        return Redirect('/registration');

    }
}
