<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Đăng ký</title>
</head>
<body>
<p style="text-align: center">
    <?php
    $message = Session::get('message');
    if ($message){
        echo $message;
        Session::put('message',null);
    }
    ?>
</p>
<div class="form">
    <form action="{{URL('/add-registration')}}" method="post">
        {{csrf_field()}}
        <h2>Đăng ký thanh vien</h2>
        <i class="fab fa-app-store-ios"></i>
        <label>Họ và tên</label>
        <input type="text" name="name">
        <label >Email</label>
        <input type="text" name="email">
        <label >Password</label>
        <input type="text" name="password">
        <input id="submit" type="submit" name="submit" value="Gửi">
    </form>

</div>
</body>
</html>